# Garage Door Opener MQTT

I just finished upgrading my garabe door opener to this. I also created the following step-by-step video tutorial below.

[![DIY Smart Garage Door Opener](https://img.youtube.com/vi/Ys27ypiDmG0/maxresdefault.jpg)](https://www.youtube.com/watch?v=Ys27ypiDmG0)

You can check my Instructable page for more details

https://www.instructables.com/id/DIY-Smart-Garage-Door-Opener-Using-ESP8266-Wemos-D/

```yaml
cover:
  - platform: mqtt
    name: "garage"
    state_topic: "garage-cover/door/status"
    command_topic: "garage-cover/door/action"
    availability_topic: "garage-cover/availability"
